# Kezbek Docker Requirenment

# **Running the Docker Compose File**

This **`docker-compose`** file sets up a production-grade environment with Kong, Konga, Portainer, RabbitMQ and three backend services. To run the environment, follow these steps:

1. git pull [https://gitlab.com/agusprg/kezbek-system/kzbk-infra](https://gitlab.com/agusprg/kezbek-system/kzbk-infra)
2. run docker compose below
    1. docker-compose -f kong-data.yml up -d
    2. docker-compose -f kong-stack.yml up -d
    3. cd backend-service
    4. docker-compose up -d
3. After all docker service running, config kong routing
4. import snapshot routing-kezbek.v1.konga.json to konga dashboard
5. Thats all, service ready to serve

**Once all the containers are up and running, you can access the following services:**

- Kong and Konga management dashboard: **[http://localhost:1337](http://localhost:1337/)**
- Portainer: **[http://localhost:9000](http://localhost:9000/)**
- Backend service : **[https://localhost](http://localhost:8000/)/api/path**

**Portainer**

![Portainer](https://gitlab.com/agusprg/kezbek-system/kzbk-infra/-/raw/main/public/portainer.png)

Portainer is a lightweight management UI which allows you to easily manage your Docker host or Swarm cluster. It provides a simple and easy-to-use web-based interface that allows you to manage your Docker resources (containers, images, volumes, networks and more) and monitor the health of your Docker services.

**Konga**

![Konga](https://gitlab.com/agusprg/kezbek-system/kzbk-infra/-/raw/main/public/konga.png)

Konga is a graphical user interface (GUI) for managing and monitoring your Kong API gateway. It provides a simple and intuitive interface for creating and configuring API endpoints, as well as for monitoring and analyzing API traffic.

****************RabbitMQ****************

![RabbitMQ](https://gitlab.com/agusprg/kezbek-system/kzbk-infra/-/raw/main/public/rabbitmq.png)

RabbitMQ is an open-source message broker software that implements the Advanced Message Queuing Protocol (AMQP). It is used to send and receive messages between different systems, and is particularly useful for enabling communication between microservices.

To stop the environment, press **`CTRL+C`** in the terminal window where the **`docker-compose`** command is running, and then run the following command to stop and remove the containers:
