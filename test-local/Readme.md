# Kezbek Zero Dependency Test

# readme

This **`docker-compose`** file sets up a local-test environment, only for kezbek main feature. 

Since the collection profile is local testing mode, so all the security was disabled, no need using apikey & jwt token and the auth related service is disabled too, only main feature

To run the environment, follow these steps:

1. git pull current repository
    
    ```bash
    git clone https://gitlab.com/agusprg/kezbek-system/kzbk-infra.git
    ```
    
2. Enter / change folder to test-local
3. Create external docker network
    
    ```bash
    docker create network docknet
    ```
    
4. Start docker compose
    
    ```bash
    docker-compose up -d
    ```
    
5. Monitoring the logs via docker logs
    
    ```bash
    docker-compose logs -f
    ```
    
6. Monitoring rabbitmq via [http://localhost:15672/](http://localhost:15672/), with username & password : guest
7. Check postman collection & documentation for testing purposed
    - [Documenter](https://documenter.getpostman.com/view/811569/2s8Z75UWFz)
    - Import Postman Collection (kezbek-local-test.postman_collection.json)
8. Start hit the api at [http://localhost:8080](http://localhost:8080/) , please follow documentation detail

****************Database****************

This service is using database hosted on free vps, if you want to change the database, define connection string database config at  

- config-consumer-notifyapplication.yml
- config-transaction/applcation.yml

After reconfig connection string, please restart the docker compose & import rule.sql to database

Happy Testing
